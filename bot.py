"""
Bot that playes this Piano tiles game:
https://www.agame.com/game/magic-piano-tiles

"""
import pyautogui
from time import sleep
import win32api, win32con

# These values may be different dependiing on you monitor, resolution and more
# Check piano_tiles_bot_coords.png for details
x0 = 500 
y = 450
shift = 100

while True:
    for i in range(4):
        if pyautogui.pixel(x:=x0+i*shift, y)[0] == 0:
            win32api.SetCursorPos((x,y))
            win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN,0,0)
            sleep(0.02)
            win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP,0,0)
